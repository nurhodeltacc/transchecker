<?php
/**
 * Translations Checker External Script
 * This script scans pages on Colnect and checks how many phrases/fields still have
 * to be translated. It is external to Colnect's site and can be run with cron.
 *
 */

require_once dirname(__FILE__).'/UrlManager.class.php';
require_once '../../../config/keys.php';
include_once dirname(__FILE__).'/checkers_common.php';

//////////////////// MAIN ACTION /////////////////////
// Connect to db
colnect_db_connect();

// Set configuration settings
$sConfigurationHost = 'https://master.colnect.lo/';
$arConfiguration = [
	'host' => $sConfigurationHost,
	'user' => 'transchecker', // This user will download all pages as a translator
	'pass' => get_special_key('SECR_transchecker'), // AW: no password needed when running on same machine
	'cacheOn' => 1,
	'urlCacheFolder' => 'cache/transchecker/',
	'retryWaitLo' => 1,
	'retryWaitHi' => 3,
	'arIgnoreFieldTranslationsUrls' => [
		'stamps/printings',
		'stamps/face_values',
		'coins/mints',
		'coins/mint_years',
		'coins/face_values',
		'bank_cards/issuers',
		'banknotes/printers',
		'banknotes/face_values',
		'beer_coasters/brands',
		'bookmarks/publishers',
		'bookmarks/artists',
		'bottlecaps/brands',
		'casino_cards/chains',
		'drink_labels/companies',
		'drink_labels/brands',
		'functional_cards/face_values',
		'gift_cards/companies',
		'gift_cards/face_values',
		'holy_cards/subjects',
		'holy_cards/places_of_worship',
		'holy_cards/issuers',
		'holy_cards/artists',
		'holy_cards/printings',
		'hotel_key_cards/chains',
		'kids_meal_toys/brands',
		'lottery_tickets/organizations',
		'lottery_tickets/face_values',
		'milk_cup_lids/companies',
		'milk_cup_lids/brands',
		'philatelic_products/issuers',
		'philatelic_products/authors',
		'phonecards/chip_types',
		'phonecards/manufacturers',
		'phonecards/face_values',
		'postcards/authors',
		'postcards/issuers',
		'sports_cards/leagues',
		'sports_cards/teams',
		'sports_cards/players',
		'sports_cards/brands',
		'stickers/companies',
		'sugar_packets/manufacturers',
		'sugar_packets/brands',
		'tea_labels/brands',
		'teabags/brands',
		'tokens/face_values',
		'trading_card_games/artists',
		'transportation_tickets/companies',
		'transportation_tickets/face_values',
		'video_games/consoles',
		'video_games/licenses',
		'video_games/publishers',
		'video_games/players',
		'cigarette_packs/brands',
	],

	// Users with language=* who should get their main language message
	// And Translators who translate more than one language
	'arAdditionalTranslators' => [
		'Polski' => [
			'quarteris',
			'atropos'
		],
		'Slovenščina' => [
			'cerbpuppy'
		],
		'Ελληνικά' => [
			'syllectis'
		],
		'نجابی' => [
			'Amjad'
		],
		'中文 (繁體)' => [
			'gaoyunyi96'
		],
		'Русский' => [
			'eleonxam'
		]
	],

	// Email subjects
	'email' => ['transchecker-report@colnect.com'],
	'onErrorEmail' => ['translations-checker-error@colnect.com'],
	'subject' => 'Missing translations automatic report '.date('Y-m-d'),
	'onErrorSubject' => 'Translations checker finished with error '.date('Y-m-d'),
	'fromEmail' => 'transchecker@colnect.com',
	'emailLangsWithMissingTranslations' => 'Languages that have missing translations: ',

	// PM parameters
	'pmApi' => $sConfigurationHost.'forums/pm_sender.php',
	'pmDefaultUsers' => ['tcawe', 'quarteris', 'syllectis'],
	'pmTitle' => "MISSING translations in [LANGUAGE] on [DATE]",
	'forcePM' => FALSE, // If TRUE, PM's will be sent to forcePMUsers
	'forcePMUsers' => ['tcawe', 'simonova'], // For to send PMs only to some users
	// Important notice: Colnect has a [b]New Translation Manager[/b]: [url=https://colnect.com/collectors/collector/syllectis]Aris Germanis [syllectis][/url]. [url=https://colnect.com/forum/viewtopic!f=112&t=60923]See the forum announcement[/url].
	'pmBody' => "Dear [NAME],

Thank you for translating Colnect to [LANGUAGE] ([LANGCODE]).

Today ([DATE]) we have seen some pages which are missing translations.
[PHRASES_DIFFERENCE] [PHRASES] and [FIELDS_DIFFERENCE] [FIELDS] need translation [COMPARISON] before.
Any missing translation in [LANGUAGE] may reduce the experience of other Colnectors using [LANGUAGE].
[b]It is VERY IMPORTANT to ensure EVERYTHING is translated the best way possible.[/b]
Please set the language selector to the language for which you have permission to translate. Set 'Translate (New)' mode and check out the pages below now:

[PAGES_LIST]

	Subscribed to the [url=https://colnect.com/forum/viewforum!f%3D112]Translations and Translators forum[/url]?
It contains up-to-date useful information for translators and is the best place for you to ask related questions.
As there may be more than one translator for [LANGUAGE], you can divide the work between translators by posting on the forum.

Let us know if you have any questions.

		Happy collecting :)",

	// DEBUG related settings
	'maxUrls' => 10000,
	'maxLangs' => 10000,
	'maxPMs' => 10000,
];

// A singleton is created and configurable parameters set
$translationsChecker = new TranslationsChecker($arConfiguration);

ignore_user_abort(true); // Keep the script running

// Run the script
$bResult = $translationsChecker->run();

// Check for errors
if (!$bResult || $translationsChecker->sError) {
	echo 'Script error -> '.$translationsChecker->sError.PHP_EOL;
	send_email($arConfiguration['fromEmail'], $arConfiguration['onErrorEmail'], $arConfiguration['onErrorSubject'], $translationsChecker->sError);
} else {
	echo 'Complete :)';
}
//////////////////////////////////////////////////////

/**
 * Class for evaluating the number of untranslated phrases/fields and sending PMs to translators
 *
 */
class TranslationsChecker
{
	// Configuration object
	private $config;
	private $arTranslators;
	private $arLangs;
	private $arUrls;
	private $arStats = []; // Stats of untranslated phrases/fields per page
	private $arPMStats = []; // Stats excluding pages with repeated phrases (for PMs)
	private $arUniqueMiscPhrases = [];
	private $arPrevStats = []; // Data from the previous run
	private $arDiffStats = []; // Difference with the previous run

	// UrlManager object
	private $urlManager;

	// To log or not to log
	private static $bOutput = true;

	// TranslationsChecker exit error
	public $sError = '';

	public function __construct(array $arConfiguration) {
		$this->config = (object) $arConfiguration;
		$this->urlManager = new UrlManager($this->config);
	}

	/**
	 * Main translations check function
	 *
	 * @return boolean false on failure
	 */
	public function run(): bool {
		global $argv;
		$bSendPM = !empty($argv[1]) && ($argv[1] === 'sendpm' || $argv[1] === 'forcepm');
		self::log('*** Starting translations_checker.php script '.($bSendPM ? 'WITH' : 'without').' sending PMs ***');

		// Login to host
		if (!$this->login()) return false;

		// Fetch translators list
		if (!$this->fetchTranslators()) return false;

		// Fetch languages list
		if (!$this->getLangs()) return false;

		// Get list of pages that will be checked
		if (!$this->getUrls()) return false;

		// Check which languages have no translators
		$this->validateTranslators();

		$iPos = 0;
		foreach ($this->arLangs as $sLangCode => $sLang) {
			if (!$this->analyzeLang($sLangCode)) return false;
			if (++$iPos >= $this->config->maxLangs) break;
		}

		$csv = $this->makeCSV();

		$sEmailBody = $this->config->emailLangsWithMissingTranslations.$this->getLanguagesMissingTranslations().'.';
		$arAttach = ['fn' => 'Translations_'.date('Y-m-d').'.csv', 'content_type' => 'application/vnd.ms-excel', 'content' => $csv];
		send_email($this->config->fromEmail, $this->config->mail, $this->config->subject, $sEmailBody, $arAttach);

		if ($argv[1] === 'forcepm') $this->config->forcePM = TRUE;

		if ($bSendPM && !$this->pmUsers()) return false;

		return true;
	}

	/**
	 * Echo a line
	 *
	 * @param string $sContent - content
	 * @param boolean [$bLineBreak]
	 * @return void
	 */
	public static function log(string $sContent, bool $bLineBreak = true): void {
		if (!self::$bOutput) return;

		// Correct new line output
		if ($bLineBreak) $sContent .= ('cli' === php_sapi_name() ? "\n" : "<br>\n");
		echo $sContent;
		flush();
	}

	/**
	 * Log in to Colnect
	 *
	 * @return boolean false on failure
	 */
	private function login(): bool {
		self::log('Load home page');

		// Visit login page
		$sPage = $this->urlManager->getUrl($this->config->host.'tool/special_login/un/'.$this->config->user, '', 1);
		$sPage = $this->urlManager->tryUnzip($sPage);

		// Check if logged in
		if (false === strpos($sPage, '<a href="/logout">')) {
			$this->sError = 'Transchecker cannot login';
			return false;
		}

		return true;
	}

	/**
	 * Log out of Colnect (to log in again)
	 *
	 * @return bool
	 */
	private function logout(): bool {
		// Visit logout page
		$sPage = $this->urlManager->getUrl($this->config->host.'logout');

		// Check if logged out
		if (false !== strpos($sPage, '<a href="/logout">')) {
			$this->sError = 'Transchecker cannot log out';
			return false;
		}

		return true;
	}

	/**
	 * Fetch translators list from Colnect and save it in an object variable
	 *
	 * @return boolean false on failure
	 */
	private function fetchTranslators(): bool {
		self::log('Load translators list');

		// Set language to English
		$this->urlManager->getUrl($this->config->host.'tool/set_language/language/en'); // Ensure English

		// Get translators
		$sPage = $this->urlManager->getCachedUrl($this->config->host.'minad.php/en/editors/permissions/filter/translators');

		// Check if fetch was successful
		if (!preg_match_all('@<td><a target="_blank" href="/collectors/collector/[^"]+?">([^<]+?)</a></td><td>lang=[^<]*</td><td><strong class="up_translator">Translator</strong>:\s*([^<]+?)(\||<)@su', $sPage, $arRows)) {
			$this->sError = 'Transchecker cannot fetch translators';
			return false;
		}

		// Get list of translators
		$arRes = [];
		$iCountRows = count($arRows[0]);
		for ($i = 0; $i < $iCountRows; $i++) {
			$name = $arRows[1][$i];
			$sLang = $arRows[2][$i];
			$sLang = preg_replace("#\s*\(.*\)\s*#u", "", $sLang);
			if ($sLang == '中文') $sLang = '中文 (繁體)'; // The above doesn't work for Chinese (Traditional)
			if (false !== $iPos = strpos($sLang, ' - ')) {
				$sLang = substr($sLang, 0, $iPos);
			}
			if ($sLang != 'ALL ') {
				if (!isset($arRes[$sLang])) $arRes[$sLang] = [];
				$arRes[$sLang][] = $name;
			}
		}

		// Add additional translators from configuration
		foreach ($this->config->arAdditionalTranslators as $sLanguage => $arTranslators) {
			if (!isset($arRes[$sLanguage])) $arRes[$sLanguage] = [];
			foreach ($arTranslators as $sTranslator) {
				$arRes[$sLanguage][] = $sTranslator;
			}
		}

		$this->arTranslators = $arRes;

		return true;
	}

	/**
	 * Fetch languages list from Colnect and save it in an object variable
	 *
	 * @return boolean false on failure
	 */
	private function getLangs(): bool {
		self::log('Load help special page (for languages)');
		$sPage = $this->urlManager->getCachedUrl($this->config->host.'en/help/collecting/special');

		// Check $sPage for <div> with languages
		if (!preg_match('@<div id="language_box">.*?</div>@su', $sPage, $sec)) {
			$this->sError = 'Transchecker cannot fetch language_box with langs';
			return false;
		}

		// Try to get languages
		//if (!preg_match_all('@<a href="[a-z\.\/_]{3,20}/tool/set_language/language/([a-z]{2})">([^<]+?)</a>@su', $sec[0], $arRows)) {
		if (!preg_match_all('@<a rel="nofollow" href="[a-z\.\/_]{3,20}/tool/set_language/language/([a-z]{2})">([^<]+?)</a>@su', $sec[0], $arRows)) {
			$this->sError = 'Transchecker cannot fetch languages';
			return false;
		}

		// Get list of languages
		$arRes = [];
		$iCountRows = count($arRows[0]);
		for ($i = 0; $i < $iCountRows; $i++) {
			$id = $arRows[1][$i];
			$sLang = $arRows[2][$i];
			$arRes[$id] = $sLang;
		}

		unset($arRes['en']); // No need to process english

		$this->arLangs = $arRes;

		return true;
	}

	/**
	 * Get list of pages that will be checked - saved in an object variable
	 *
	 * @return boolean false on failure
	 */
	private function getUrls(): bool {
		$arLangKeys = array_keys($this->arLangs);
		$sLang = $arLangKeys[0];

		self::log("Set $sLang language");
		$this->urlManager->getUrl($this->config->host."tool/set_language/language/$sLang"); // Uncached call (otherwise Colnect will redirect)

		self::log("Load $sLang pages to translate");

		// Get pages to translate
		$sPage = $this->urlManager->getCachedUrl($this->config->host."$sLang/help/collecting/pages_to_translate");
		if (!preg_match('@<div id="part1">.*?</div>\s*</div>@su', $sPage, $sec)) {
			$this->sError = 'Transchecker cannot fetch div with urls';
			return false;
		}

		if (!preg_match_all('@<a[^>]+?href="/'.$sLang.'/([^"]+?)">@su', $sec[0], $arRows)) {
			$this->sError = 'Transchecker cannot fetch urls';
			return false;
		}

		// Remove duplicates if any
		$this->arUrls = array_flip(array_flip($arRows[1]));

		return true;
	}

	/**
	 * Check which languages have no translators (thus nobody will receive a PM)
	 */
	private function validateTranslators() {
		$bNoTranslatorsForLanguage = false;

		// Check languages for translators
		foreach ($this->arLangs as $sLanguage) {
			if (!isset($this->arTranslators[$sLanguage])) {
				self::log("No translators for $sLanguage");
				$bNoTranslatorsForLanguage = true;
			}
		}
		if ($bNoTranslatorsForLanguage) {
			self::log('WARNING: some languages have no translators!');
		}
	}

	/**
	 * Try to find one of many needles in a haystack (from StringOps class on Colnect)
	 *
	 * @param array $needles_array
	 * @param string $haystack
	 * @return false if not found or key of the first match
	 */
	private static function array_first_ipos($needles_array, $haystack) {
		foreach ($needles_array as $k => $v) {
			if (false !== stripos($haystack, $v)) return $k;
		}
		return false;
	}

	/**
	 * Get a specific Colnect page and check how many translations are missing.
	 * Updates object stats variable
	 *
	 * @param string $sLangCode
	 * @param string $sUrl
	 * @param string $sPage
	 * @param string [$sError]
	 * @param int $iExecTime
	 * @return boolean
	 */
	private function analyzePage(string $sLangCode, string $sUrl, string $sPage, string $sError = '', int $iExecTime): bool {
		// If page wasn't fetched properly, mark it as unavailable
		if (!$this->isAvailablePage($sUrl, $sPage) || $sError || (false === strpos($sPage, '</html>'))) {
			$this->arStats[$sLangCode][$sUrl] = ['UNAVAIL', 'UNAVAIL'];
			return true;
		}

		// Check if we got logged out somehow
		if (false === strpos($sPage, '/logout')) {
			$this->sError = 'Not logged in on page fetched! Consider deleting last files in cache.';
			return false;
		}

		// Check for missing translations
		$arPageStats = $this->getPageStats($sUrl, $sPage);
		$iPhrases = $arPageStats['iPhrases'];
		$iFields = $arPageStats['iFields'];
		$iUniquePhrases = $this->addUniqueMiscPhrases($arPageStats['phraseValues']); // Look for unique phrases

		// Check if there are some links to load and look for missing translations in them
		if ($arPageStats['links']) {
			foreach ($arPageStats['links'] as $link) {
				$sLinkUrl = substr($this->config->host, 0, -1).$link->getAttribute("data-url");
				$sLinkPage = $this->urlManager->getCachedUrl($sLinkUrl);
				if (!$this->isAvailablePage($sLinkUrl, $sLinkPage)) {
					continue;
				}

				// Check link for missing translations
				$arLinkStats = $this->getPageStats($sLinkUrl, $sLinkPage);
				$iPhrases += $arLinkStats['iPhrases'];
				$iFields += $arLinkStats['iFields'];
				$iUniquePhrases += $this->addUniqueMiscPhrases($arLinkStats['phraseValues']); // Add unique phrases from this link
			}
		}

		// Check if untranslated phrases and fields were found and add them to DB
		if ($iPhrases + $iFields > 0) {
			if (!colnect_db_connect()) { // Check if disconnected from DB and reconnect
				colnect_db_connect();
			}
			DbTools::query("INSERT INTO tr_check_pages SET timeadded = NOW(), langCode = :lang, url = :url, downMSec = :exec_time, misPhrases = :phrases, misFields = :fields", 0, 1,
				['lang' => $sLangCode, 'url' => $sUrl, 'exec_time' => $iExecTime, 'phrases' => $iPhrases, 'fields' => $iFields]);
			$this->arStats[$sLangCode][$sUrl] = [$iPhrases, $iFields];
		}

		// Check if untranslated unique phrases and fields were found
		if ($iUniquePhrases + $iFields > 0) {
			$this->arPMStats[$sLangCode][$sUrl] = [$iPhrases, $iFields];
		}

		return true;
	}

	/**
	 * Check if page was fetched properly and it's content is available
	 *
	 * @param string $sUrl
	 * @param string $sPage
	 * @return boolean
	 */
	private function isAvailablePage(string $sUrl, string $sPage) {
		if (empty($sPage) || ('main/404page' === $sUrl) || (false === strpos($sPage, 'HTTP/1.1 200 OK'))) {
			return false;
		}
		return true;
	}

	/**
	 * Function to check page and page's links for missing translations
	 *
	 * @param string $sUrl
	 * @param string $sPage
	 * @return array $arPageStats
	 */
	private function getPageStats(string $sUrl, string $sPage): array {
		$arPageStats['phraseValues'] = [];

		// Use DOM to check for missing translations
		$dom = new DOMDocument();
		@$dom->loadHTML($sPage);
		$xpath = new DOMXPath($dom);

		// Check for missing miscPhrases and save their values to array
		$phraseValues = $xpath->evaluate("//*[contains(concat(' ',normalize-space(@class),' '),' tr_m ')]");
		$arPageStats['iPhrases'] = $phraseValues->length;
		foreach ($phraseValues as $phraseValue) {
			$arPageStats['phraseValues'][] = $phraseValue->nodeValue;
		}

		// Check if url is not one from the ignore list for fields' counting and look for missing fields
		if (false !== self::array_first_ipos($this->config->arIgnoreFieldTranslationsUrls, $sUrl)) {
			$arPageStats['iFields'] = 0;
		} else {
			$fieldValues = $xpath->evaluate("//*[contains(concat(' ',normalize-space(@class),' '),' tr_f ')]");
			$arPageStats['iFields'] = $fieldValues->length;
		}

		// Check if there are some links to load and fetch
		$links = $xpath->evaluate("//*[contains(concat(' ',normalize-space(@class),' '),' load_me ')]");
		$arPageStats['links'] = $links;

		return $arPageStats;
	}

	/**
	 * Check how many unique miscPhrases to add after page fetching
	 *
	 * @param array $arPhraseValues
	 * @return integer - the number of unique misc phrases to add
	 */
	private function addUniqueMiscPhrases(array $arPhraseValues): int {
		$iCounter = 0;
		foreach ($arPhraseValues as $value) {
			if (!isset($this->arUniqueMiscPhrases[$value])) {
				$this->arUniqueMiscPhrases[$value] = true;
				$iCounter++;
			}
		}

		return $iCounter;
	}

	/**
	 * Check all pages of a single language
	 *
	 * @param string $sLangCode - language code
	 * @return boolean false on failure
	 */
	private function analyzeLang(string $sLangCode): bool {
		self::log("Set $sLangCode language and ensure translate (new) mode");
		$this->urlManager->getUrl($this->config->host."tool/set_language/language/$sLangCode");
		$this->urlManager->getUrl($this->config->host.'tool/translator_mode/mode/new');
		$this->arUniqueMiscPhrases = [];

		// Find datetime and total number of untranslated phrases and fields from the previous run
		// Find datetime of the previous run of transchecker script
		$prevRunTimeQuery = DbTools::query("SELECT MAX(timeadded) AS max FROM tr_check_pages WHERE langCode = :lang", 0, 1, ['lang' => $sLangCode]);
		$arPrevRunTime = $prevRunTimeQuery->fetchAll(PDO::FETCH_ASSOC); // Converting to array
		$iPrevRunTime = $arPrevRunTime[0]['max']; // Time of the previous run of the script

		// Find number of untranslated phrases from the previous run
		$prevPhrasesQuery = DbTools::query("SELECT SUM(misPhrases) AS sum FROM tr_check_pages WHERE langCode = :lang AND timeadded > (:prev_time - INTERVAL 5 MINUTE)", 0, 1,
			['lang' => $sLangCode, 'prev_time' => $iPrevRunTime]); // 5-min interval is taken into account as rows could be added to the database with some time interval
		$arPrevPhrases = $prevPhrasesQuery->fetchAll(PDO::FETCH_ASSOC); // Converting to array
		$this->arPrevStats[$sLangCode][] = $arPrevPhrases[0]['sum']; // Number of missing phrases from the previous run

		// Find number of untranslated fields from the previous run
		$prevFieldsQuery = DbTools::query("SELECT SUM(misFields) AS sum FROM tr_check_pages WHERE langCode = :lang AND timeadded > (:prev_time - INTERVAL 5 MINUTE)", 0, 1,
			['lang' => $sLangCode, 'prev_time' => $iPrevRunTime]);
		$arPrevFields = $prevFieldsQuery->fetchAll(PDO::FETCH_ASSOC); // Converting to array
		$this->arPrevStats[$sLangCode][] = $arPrevFields[0]['sum']; // Number of missing fields from the previous run

		// Get curl_multi results - all the fetched pages
		$arCurlResults = $this->urlManager->getMultipleUrls($this->config->host."$sLangCode/", $this->arUrls);

		// Analyze all pages
		foreach ($arCurlResults as $sUrl => $arPage) {
			if (!$this->analyzePage($sLangCode, $sUrl, $arPage['sPage'], $arPage['sError'], $arPage['iExecTime'])) return false;
		}

		return true;
	}

	/**
	 * Create report CSV file
	 *
	 * @return string - resulting content
	 */
	private function makeCSV() {
		$csv = fopen('php://temp/maxmemory:'.(5 * 1024 * 1024), 'r+');

		$arLookupLang = array_flip($this->arLangs); // Seek stats by code

		// Formatting of language names (e.g. (en) English)
		$arLangs = array_keys($this->arLangs);
		foreach ($arLangs as $k => $v) {
			$arLangs[$k] = '('.$v.') '.$this->arLangs[$v];
		}

		// Display of table header
		$colsHeader = array_merge(['urls'], $arLangs);
		fputcsv($csv, $colsHeader);

		// Prepare number of missing phrases+fields for each lang for each url
		$arLangs = array_values($this->arLangs);
		foreach ($this->arUrls as $sUrl) { // Rows
			$arStats = [];
			foreach ($arLangs as $sLang) { // Cols
				$sLangCode = $arLookupLang[$sLang];
				if (isset($this->arStats[$sLangCode][$sUrl]) && $this->arStats[$sLangCode][$sUrl] !== ['UNAVAIL', 'UNAVAIL']) { // If page was fetched properly
					$arData = $this->arStats[$sLangCode][$sUrl];
					$arStats[] = (int) ($arData[0] + $arData[1]); // Sum of missing phrases and fields for language $sLang for url $sUrl
				} elseif (isset($this->arStats[$sLangCode][$sUrl]) && $this->arStats[$sLangCode][$sUrl] === ['UNAVAIL', 'UNAVAIL']) {
					$arStats[] = 'UNAVAIL'; // If page wasn't fetched properly
				} else {
					$arStats[] = '';
				}
			}
			$cols = array_merge([$sUrl], $arStats); // Filling cells of the table with $arStats data
			fputcsv($csv, $cols);
		}

		// Total statistics for current transchecker script run
		$arStats = $arStatsF = $arStatsP = $arDiffP = $arDiffF = [];
		foreach ($arLangs as $sLang) {
			$iLangTotalPh = 0; // Counter of total number of missing phrases for $sLang
			$iLangTotalF = 0; // Counter of total number of missing fields for $sLang
			foreach ($this->arUrls as $sUrl) {
				$sLangCode = $arLookupLang[$sLang];
				if (isset($this->arStats[$sLangCode][$sUrl]) && $this->arStats[$sLangCode][$sUrl] !== ['UNAVAIL', 'UNAVAIL']) { // Check that page was fetched properly
					$arData = $this->arStats[$sLangCode][$sUrl];
					$iLangTotalPh += (int) $arData[0]; // Total missing phrases for $sLang
					$iLangTotalF += (int) $arData[1]; // Total missing fields for $sLang
				}
			}

			// Saving data to final statistics arrays
			$arStats[] = $iLangTotalPh + $iLangTotalF; // Sum of missing phrases+fields
			$arStatsP[] = $iLangTotalPh;; // Missing phrases
			$arStatsF[] = $iLangTotalF; // Missing fields

			// Compare data from the previous run with the current one for $sLang
			$iPrevMisPhrases = $this->arPrevStats[$sLangCode][0]; // Previous run missing phrases
			$iPrevMisFields = $this->arPrevStats[$sLangCode][1]; // Previous run missing fields
			if ($iPrevMisPhrases || $iPrevMisFields) {
				$iDiffMisPhrases = (int) ($iLangTotalPh - $iPrevMisPhrases); // Difference of missing phrases
				$iDiffMisFields = (int) ($iLangTotalF - $iPrevMisFields); // Difference of missing fields
			} else { // If data of the previous run wasn't defined
				$iDiffMisPhrases = '';
				$iDiffMisFields = '';
			}

			// Saving difference with the prev run to final statistics arrays
			$arDiffP[] = $iDiffMisPhrases; // Difference of missing phrases
			$arDiffF[] = $iDiffMisFields; // Difference of missing fields
			$this->arDiffStats[$sLangCode] = [$iDiffMisPhrases, $iDiffMisFields]; // Saving difference (to use in SendPM function)
		}

		// Display of total statistics at the end of the table (specified for each language)
		// Current sum of missing phrases and fields
		$cols = array_merge(['Total'], $arStats);
		fputcsv($csv, $cols);

		// Current number of missing fields
		$cols = array_merge(['Fields Total'], $arStatsF);
		fputcsv($csv, $cols);

		// Difference of missing fields with the previous run
		$cols = array_merge(['Fields Change'], $arDiffF);
		fputcsv($csv, $cols);

		// Current number of missing phrases
		$cols = array_merge(['Phrases Total'], $arStatsP);
		fputcsv($csv, $cols);

		// Difference of missing phrases with the previous run
		$cols = array_merge(['Phrases Change'], $arDiffP);
		fputcsv($csv, $cols);
		fputcsv($csv, $colsHeader); // Write header again (closer to totals)

		// Put it all in a variable
		rewind($csv);
		return stream_get_contents($csv);
	}

	/**
	 * Removes spaces and other characters from a message
	 *
	 * @param string $s
	 * @return string $s
	 */
	private function myTrim(string $s): string {
		if ('' === $s) return '';

		$arReplacement = [
			"&amp;" => "&",
			"&#039;" => "'",
			"&#034;" => '"',
			"<br>" => "\n",
			"&nbsp;" => " ",
			"\t" => "",
			"\n" => "",
			"\r" => "",
		];
		$s = strtr($s, $arReplacement);
		$s = preg_replace("/\s+/", " ", $s);
		$s = trim($s);

		return $s;
	}

	/**
	 * Send phpBB PM to translator using external request
	 *
	 * @param string $messageTpl - PM message template
	 * @param string $sLang - language name
	 * @param string $sLangCode - language code
	 * @param string $username - translator username
	 * @param array $arUrls - URLs missing translations
	 * @return boolean false on failure
	 */
	private function sendPM(string $messageTpl, string $sLang, string $sLangCode, string $username, array $arUrls): bool {
		$sPagesList = '';
		$sUrlPrefix = 'zh' === $sLangCode ? 'https://colnect.cn/'.$sLangCode : $this->config->host.$sLangCode;

		// Prepare pages list
		foreach ($arUrls as $k => $v) {
			$iPhrases = $v[0];
			$iFields = $v[1];
			$sPagesList .= '[url]'.$sUrlPrefix.'/'.$k."[/url]"
				.(empty($iPhrases) ? '' : " Phrases[$iPhrases]")
				.(empty($iFields) ? '' : " Fields[$iFields]")
				."\n";
		}

		// Difference of number of untranslated phrases and fields compared to the previous run
		// Prepare formatted data for PMs
		foreach($this->arDiffStats[$sLangCode] as $iDiff) {
			$arDiff[] = $iDiff > 0 ? $iDiff.' more' : ($iDiff < 0 ? abs($iDiff).' less' : ' same number of');
		}
		$iPhrasesDiff = $this->arDiffStats[$sLangCode][0];
		$iFieldsDiff = $this->arDiffStats[$sLangCode][1];
		$diffPh = 0 == $iPhrasesDiff ? 'The'.$arDiff[0] : $arDiff[0];
		$diffF = 0 == $iFieldsDiff ? 'the'.$arDiff[1] : $arDiff[1];
		$sPhrases = 1 === abs($iPhrasesDiff) ? 'phrase' : 'phrases';
		$sFields = 1 === abs($iFieldsDiff) ? 'field' : 'fields';
		$comp = 0 == $iFieldsDiff ? 'as' : 'than';

		$arReplacement = [
			'[NAME]' => $username,
			'[LANGUAGE]' => $sLang,
			'[LANGCODE]' => $sLangCode,
			'[PHRASES_DIFFERENCE]' => $diffPh,
			'[FIELDS_DIFFERENCE]' => $diffF,
			'[PHRASES]' => $sPhrases,
			'[FIELDS]' => $sFields,
			'[COMPARISON]' => $comp,
			'[PAGES_LIST]' => $sPagesList,
			'[DATE]' => date("d-m-Y")
		];

		// Replace message
		$sTxt = strtr($messageTpl, $arReplacement);

		// Replace title
		$sTitle = strtr($this->config->pmTitle, $arReplacement);

		$arUserdata = [
			'user' => $username,
			'subject' => $sTitle,
			'message' => $sTxt
		];

		$sPmApi = $this->config->pmApi;
		self::log("Send PM to [$arUserdata[user]] using POST to $sPmApi");
		$sPage = $this->urlManager->getUrl($this->config->pmApi, http_build_query($arUserdata), 1);

		if (strpos($sPage, 'OK|added') === false) {
			preg_match('@\berror\|(.*)@s', $sPage, $arMatches);
			$sError = $this->myTrim($arMatches[1] ?? '');
			self::log('ERROR->'.$sError);
			if ('must login as user' === $sError) {
				$this->login();
			}
			return false;
		}

		self::log('Sent OK');

		return true;
	}

	/**
	 * Send phpBB PM to all translators
	 *
	 * @return boolean false on failure
	 */
	private function pmUsers(): bool {
		// Different user for PMs
		$this->config->user = get_special_key('LOGIN_t_manager'); // All PMs will be sent by this user
		$this->config->pass = get_special_key('SECR_t_manager'); // AW: no password needed when running on same machine
		if (!$this->logout() || !$this->login()) return false;

		$arLangCodes = array_keys($this->arLangs);
		$iPos = 0;

		foreach ($arLangCodes as $sLangCode) {
			if (empty($this->arPMStats[$sLangCode])) continue;
			$arUrls = $this->arPMStats[$sLangCode];

			$sLang = $this->arLangs[$sLangCode]; // Lookup translators by long title (language)

			$arUsers = $this->arTranslators[$sLang] ?? $this->config->pmDefaultUsers;
			if ($this->config->forcePM) {
				$arUsers = $this->config->forcePMUsers;
			}

			foreach ($arUsers as $sUsername) {
				if (++$iPos > $this->config->maxPMs) break;

				// Continue at PM error
				if (!$this->sendPM($this->config->pmBody, $sLang, $sLangCode, $sUsername, $arUrls)) {
					$iSec = mt_rand($this->config->retryWaitLo, $this->config->retryWaitHi);
					self::log("FAIL. PM retry in $iSec seconds");
					sleep($iSec);
					if (!$this->sendPM($this->config->pmBody, $sLang, $sLangCode, $sUsername, $arUrls)) {
						$this->sError .= "Error occurred while sending PM to $sUsername ($sLang language)\n";
						continue;
					}
				}

				$sContent = join("\n", array_keys($arUrls));
				$sUsername = addslashes($sUsername);
				DbTools::query("INSERT INTO tr_check_pms SET timeadded = NOW(), langCode = :lang, translator = :uname, content = :content", 0, 1,
					['lang' => $sLangCode, 'uname' => $sUsername, 'content' => $sContent]);
			}
		}
		return true;
	}

	/**
	 * Get string with languages that have missing translations
	 *
	 * @return string
	 */
	private function getLanguagesMissingTranslations(): string {
		$arResult = [];

		$arLangCodes = array_keys($this->arStats);
		foreach ($arLangCodes as $sLangCode) {
			$arResult[] = $this->arLangs[$sLangCode];
		}

		return implode(', ', $arResult);
	}
}