<?php
/**
 * Class for fetching URLs, handling their unzipping and caching
 * Used as part of Transchecker script (translations_checker.php - script to check pages for translations to all active languages)
 */
class UrlManager
{
	private $config;
	private $cookieFile;
	public $sCurlError = '';
	public $iExecutionTime;
	private $arCurlHeader = [
		'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Language: en-us,en;q=0.5',
		'Accept-Charset: utf-8, windows-1251;q=0.7,*;q=0.7',
		'Keep-Alive: 115', 'Connection: keep-alive',
		'Pragma: no-cache',
		'Cache-Control: no-cache', 'Accept-Encoding: gzip,deflate'
	];

	/**
	 * Construct new UrlManager
	 *
	 * @param object $configuration
	 */
	public function __construct($configuration) {
		$this->config = $configuration;
		$this->generateCookieFile();
	}

	/**
	 * Make sure the cookie file is removed
	 */
	public function __destruct() {
		if (!$this->config->cacheOn) unlink($this->cookieFile);
	}

	/**
	 * Create or use existing cookie file
	 *
	 * @return void
	 */
	private function generateCookieFile(): void {
		// Decide where to place cookie file
		if ($this->config->cacheOn) {
			if (!file_exists($this->config->urlCacheFolder)) mkdir($this->config->urlCacheFolder, 0777, true);
			$this->cookieFile = $this->config->urlCacheFolder.'cookies.txt';
		} else {
			$this->cookieFile = tempnam('/tmp', 'cookies');
		}
	}

	/**
	 * Fetch/post a request using curl
	 *
	 * @param string $sUrl - URL to GET/POST
	 * @param string [$sData] - optional POST data
	 * @param integer [$iHead]
	 * @param integer [$iFollow]
	 * @param string [$sReferer]
	 * @return string
	 */
	public function getURL(string $sUrl, string $sData = '', int $iHead = 0, int $iFollow = 1, string $sReferer = ''): string {
		$ch = curl_init($sUrl);

		// Set curl options
		if (empty($sReferer)) $sReferer = $sUrl;
		$ch = $this->setCurlOptions($ch, $sData, $iHead, $iFollow, $sReferer);

		TranslationsChecker::log($sUrl, false);
		$iStartTS = microtime(true);
		$sPage = curl_exec($ch);

		$this->sCurlError = curl_error($ch);
		curl_close($ch);
		if ($this->sCurlError) {
			TranslationsChecker::log("Curl error for [$sUrl]\nCurl report:".$this->sCurlError);
			return '';
		}

		$this->iExecutionTime = (int)((microtime(true) - $iStartTS) * 1000);
		TranslationsChecker::log(' '.$this->iExecutionTime.'ms');
		if ($this->iExecutionTime > 2000) TranslationsChecker::log("Took us $this->iExecutionTime msec to fetch $sUrl!");

		$sPage = $this->tryUnzip($sPage);

		return $sPage;
	}

	/**
	 * Fetch/post a request using curl if a cached copy of the result isn't found
	 *
	 * @param string $sUrl - URL to GET/POST
	 * @param string [$sData] - optional POST data
	 * @param integer [$iHead]
	 * @param integer [$iFollow]
	 * @return string $sPage
	 */
	public function getCachedUrl(string $sUrl, string $sData = '', int $iHead = 1, int $iFollow = 1): string {
		$sCacheFile = $this->getCacheFileName($sUrl, $sData);

		// Take page from cache if cache is on and cached file exists
		if ($this->config->cacheOn && file_exists($sCacheFile) && (time() - filemtime($sCacheFile) < 3600 * 72 /* 72 hours */)) {
			return file_get_contents($sCacheFile);
		} else {

			// Fetch url if cache is off or cached file doesn't exist
			$sPage = $this->getURL($sUrl, $sData, $iHead, $iFollow, $sReferer = '');
			$sPage = $this->tryUnzip($sPage);
		}

		// Save page to cache if needed
		if ($this->config->cacheOn && $sPage && !$this->sCurlError && false !== strpos($sPage, 'HTTP/1.1 200 OK')) {
			file_put_contents($sCacheFile, $sPage);
		}

		return $sPage;
	}

	/**
	 * Get urls from array with curl_multi_init
	 *
	 * @param string $sHost
	 * @param array $arUrls
	 * @param string [$sData] - optional POST data
	 * @param int [$iHead]
	 * @param int [$iFollow]
	 * @return array
	 */
	public function getMultipleUrls(string $sHost, array $arUrls, string $sData = '', int $iHead = 1, int $iFollow = 1): array {
		// Set variables
		$arUrlsToFetch = $arCurlResults = $ch = [];
		$mh = curl_multi_init();
		$iUrlCounter = 0; // Urls_to_fetch counter

		// Look through the urls to create array of urls to fetch
		foreach ($arUrls as $sUrl) {

			// Check cache
			$sCacheFile = $this->getCacheFileName($sHost.$sUrl, $sData);
			if ($this->config->cacheOn && file_exists($sCacheFile) && (time() - filemtime($sCacheFile) < 3600 * 72)) {
				$sPage = file_get_contents($sCacheFile); // Fetch from cache

				// Put unzipped page to return array
				$arCurlResults[$sUrl] = [
					'sPage' => $this->tryUnzip($sPage),
					'sError' => '',
					'iExecTime' => 0
				];
			} else {
				$arUrlsToFetch[] = $sUrl; // Mark url to be fetched

				// Init curl
				$currentHandler = $ch[] = curl_init($sHost.$sUrl);
				$currentHandler = $this->setCurlOptions($currentHandler, $sData, $iHead, $iFollow);
				curl_multi_add_handle($mh, $currentHandler);

				// Check for limit on max urls
				$iUrlCounter++;
				if ($iUrlCounter >= $this->config->maxUrls) break;

			}
		}

		$iStartTS = microtime(true);

		// Execute multiple curl requests
		$iRunning = 0;
		do {
			curl_multi_exec($mh, $iRunning);
		} while ($iRunning > 0);

		$iCurlMultiExecTime = (int)((microtime(true) - $iStartTS) * 1000);

		TranslationsChecker::log('Curl_multi_exec execution time: '.$iCurlMultiExecTime.'ms'.PHP_EOL, false);

		// Get curl execution results
		foreach ($arUrlsToFetch as $iUrlToFetch => $sUrlToFetch) {
			$currentHandler = $ch[$iUrlToFetch];
			$curlError = curl_error($currentHandler);
			$sPage = curl_multi_getcontent($currentHandler);
			$iExecTime = $iCurlMultiExecTime;

			// Remove and close the handle
			curl_multi_remove_handle($mh, $currentHandler);
			curl_close($currentHandler);

			$sPage = $this->tryUnzip($sPage);

			// If failed or page was fetched with errors, try again
			if (empty($sPage)
				|| (false === strpos($sPage, '</html>'))
				|| (false === strpos($sPage, 'HTTP/1.1 200 OK'))
				|| $curlError) {
				$iSec = mt_rand($this->config->retryWaitLo, $this->config->retryWaitHi);
				TranslationsChecker::log("FAIL. Will retry in $iSec seconds");
				sleep($iSec);
				$sPage = $this->getUrl($sHost.$sUrlToFetch, '', 1);
				$curlError = $this->sCurlError;
				$iExecTime = $this->iExecutionTime;
			}

			// Put unzipped page as sPage, curlError as sError
			$arCurlResults[$sUrlToFetch] = [
				'sPage' => $sPage,
				'sError' => $curlError,
				'iExecTime' => $iExecTime
			];

			TranslationsChecker::log($sUrlToFetch.PHP_EOL, false);

			// Log curl error
			if ($curlError) {
				TranslationsChecker::log("Curl error for [$sUrlToFetch]\nCurl report:".$curlError);
				continue;
			}

			// Check if page need to be stored to cache and do it
			$sCacheFile = $this->getCacheFileName($sHost.$sUrlToFetch, $sData);
			if ($this->config->cacheOn && (!file_exists($sCacheFile) || (time() - filemtime($sCacheFile) > 3600 * 72))
				&& !$this->sCurlError && false !== strpos($sPage, 'HTTP/1.1 200 OK')) {
				file_put_contents($sCacheFile, $sPage);
			}
		}

		curl_multi_close($mh); // Close multi curl

		return $arCurlResults;
	}

	/**
	 * Possibly unzip a ZIP encoded page by breaking its header part and using a temp file
	 *
	 * @param string $sPage
	 * @return string $sPage
	 */
	public function tryUnzip(string $sPage): string {
		if (false !== strpos($sPage, 'Content-Encoding: gzip')) {
			$fname = tempnam('/tmp', 'bp');
			preg_match_all('#(HTTP/\d\.\d.*?$.*?\r?\n\r?\n)#ims', $sPage, $head);
			$sPage = preg_replace('#HTTP/\d\.\d.*?$.*?\r?\n\r?\n#ims', '', $sPage);
			file_put_contents($fname, $sPage);
			$sPage = join('', $head[1])."\n\n".$this->ungz($fname);
			unlink($fname);
		}

		return $sPage;
	}

	/**
	 * Read and unzip a file
	 *
	 * @param string $sFileName - filename
	 * @return string
	 */
	public function ungz(string $sFileName): string {
		$zp = gzopen($sFileName, 'r');
		$sPage = gzread($zp, 10000000);
		gzclose($zp);

		return $sPage;
	}

	/**
	 * Get hashed name for file in cache
	 *
	 * @param string $sUrl
	 * @param string [$sData]
	 * @return string
	 */
	private function getCacheFileName(string $sUrl, string $sData = ''): string {
		$sHash = md5($sUrl.$sData);
		$cacheFile = self::CACHEFOLDER.$sHash.'.html';

		return $cacheFile;
	}

	/**
	 * Set CURL options
	 *
	 * @param resource $ch
	 * @param string [$sData]
	 * @param int [$iHead]
	 * @param int [$iFollow]
	 * @param string [$sReferer]
	 * @return resource
	 */
	private function setCurlOptions($ch, string $sData = '', int $iHead = 0, int $iFollow = 1, string $sReferer = '') {
		curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arCurlHeader);
		curl_setopt($ch, CURLOPT_HEADER, $iHead);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $iFollow);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; Transchecker/1.0; +https://colnect.com/)');
		//curl_setopt ($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
		curl_setopt($ch, CURLOPT_ENCODING, '');
		curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieFile);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookieFile);

		if (!empty($sReferer)) {
			curl_setopt($ch, CURLOPT_REFERER, $sReferer);
		}

		if ($sData) {
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $sData);
		}

		return $ch;
	}
}